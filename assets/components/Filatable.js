import React from 'react';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
class Filatable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            eliminado: false,
        };
        this.eliminar = this.eliminar.bind(this);
    }
        async eliminar() {
            const resultado = await Swal.fire({
                title: 'Confirmación',
                text: `¿Eliminar "${this.props.listarv.id}"?`,
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3298dc',
                cancelButtonColor: '#f14668',
                cancelButtonText: 'No',
                confirmButtonText: 'Sí, eliminar'
            });
            // Si no confirma, detenemos la función
            if (!resultado.value) {
                return;
            }
            const respuesta = await fetch(`/api/delete_api_viajes/${this.props.listarv.id}`, {
                method: "DELETE",
            });
            const exitoso = await respuesta.json();
            if (exitoso) {
                toast('Viajes eliminado ', {
                    position: "top-left",
                    autoClose: 2000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
                this.setState({
                    eliminado: true,
                });
            } else {
                toast.error("Error eliminando. Intenta de nuevo");
            }
        }
    render() {
        if (this.state.eliminado) {
            return null;
        }
        return (
            <tr>
                <td>{this.props.listarv.codigo}</td>
                <td>{this.props.listarv.n_plaza}</td>
                <td>{this.props.listarv.origen}</td>
                <td>{this.props.listarv.destino}</td>
                <td>{this.props.listarv.precio}</td>
                <td>
                    <Link to={`/editar/${this.props.listarv.id}`} className="btn btn-info">Editar</Link>
                </td>
                <td>
                    <button onClick={this.eliminar} className="btn btn-danger">Eliminar</button>
                </td>
            </tr>
        );
    }
}

export default Filatable;