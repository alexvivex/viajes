import React from 'react';
import { Link } from 'react-router-dom';
import '../styles/bootstrap.min.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'

class AddViajes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listarv: [],
            addviajes: {
                "cedula": "",
                "nombre": "",
                "fecha": "",
                "telefono": "",
                "id_viaje": "",       
            },
        };

        
        // Indicarle a las funciones a quién nos referimos con "this"
        this.manejarCambio = this.manejarCambio.bind(this);
        this.manejarEnvioDeFormulario = this.manejarEnvioDeFormulario.bind(this);
    }
    
    async componentDidMount() {
        const respuesta = await fetch('/api/get_api_viajes');
        const listarv = await respuesta.json();
        this.setState({
            listarv: listarv,
        });
    }
    render() {
        return (
            <div className="col-md-5">
                <h1 className="is-size-3">Tomar Boleto</h1>
                <ToastContainer></ToastContainer>
                <form className="field" onSubmit={this.manejarEnvioDeFormulario}>
                    <div className="form-group">
                        <label className="label">Cedula:</label>
                        <input autoFocus required placeholder="Cedula" type="text" id="cedula" onChange={this.manejarCambio} value={this.state.addviajes.cedula} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label className="label">Nombre:</label>
                        <input required placeholder="Nombre y Apellidp" type="text" id="nombre" onChange={this.manejarCambio} value={this.state.addviajes.nombre} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label className="label">Fecha:</label>
                        <input required placeholder="Fecha del vuelo" type="date" id="fecha" onChange={this.manejarCambio} value={this.state.addviajes.fecha} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label className="label">Telefono:</label>
                        <input required placeholder="telefono" type="text" id="telefono" onChange={this.manejarCambio} value={this.state.addviajes.telefono} className="form-control" />
                    </div>

                    <div className="form-group">
                        <label className="label" htmlFor="calificacion">Vuelo:</label>
                        <select name="id_viaje" onChange={this.manejarCambio} id="id_viaje"  className='form-control'>
                            {this.state.listarv.map((listarv) => (
                            <option value={listarv.id}>{listarv.origen + "-" + listarv.destino}</option>
                        ))}
                        </select>
                    </div>
                    
                    <div className="form-group">
                        <br></br>
                        <button className="btn btn-success">Guardar</button>
                        &nbsp;
                        <Link to="/Home" className="btn btn-primary">Volver</Link>
                    </div>
                </form>
            </div>
        );
    }
    async manejarEnvioDeFormulario(evento) {

        evento.preventDefault();
        // Codificar nuestro viaje como JSON

        const cargaUtil = JSON.stringify(this.state.addviajes);

        const respuesta = await fetch(`/api/post_api`, {
            method: "POST",
            body: cargaUtil,
        });
        const exitoso = await respuesta.json();
        if (exitoso) {
            toast('Viaje guardado', {
                position: "top-left",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            this.setState({
                addviajes: {
                    cedula: "",
                    nombre: "",
                    fecha: "",
                    telefono: "",
                    id_viaje: "",
                }
            });
        } else {
            toast.error("Error guardando. Intenta de nuevo");
        }
    }
    manejarCambio(evento) {
        // Extraer la clave del estado que se va a actualizar, así como el valor
        const clave = evento.target.id;
        let valor = evento.target.value;
        this.setState(state => {
            const addviajesactualizado = state.addviajes;

            addviajesactualizado[clave] = valor;
            return {
                viajes: addviajesactualizado,
            }
            
        });
    }
}

export default AddViajes;
