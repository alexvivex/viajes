
import React from 'react';
import Filatable from './Filatable';
import '../styles/bootstrap.min.css';


class Listar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listarv: [],
        };
    }
    async componentDidMount() {
        const respuesta = await fetch('/api/get_api_viajes');
        const listarv = await respuesta.json();
        this.setState({
            listarv: listarv,
        });
    }
    render() {
        return (
            <div>
                <div className='container'>
                    <div className="col-md-12">
                       
                    </div>
                </div>
                <hr>
                </hr>
                <br></br>
                <h1 className="col-md-5">Listar Viajes</h1>
                <div className="table-container">
                    <table className="table is-fullwidth is-bordered">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>N Plazas</th>
                                <th>Origen del Viaje</th>
                                <th>Destino del Viaje</th>
                                <th>Precio</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.listarv.map(listarv => {
                                return <Filatable key={listarv.id} listarv={listarv}></Filatable>;
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default Listar;
