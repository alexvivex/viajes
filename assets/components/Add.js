import React from 'react';
import { Link } from 'react-router-dom';
import '../styles/bootstrap.min.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Add extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            viajes: {
                "codigo": "",
                "n_plaza": "",
                "origen": "",
                "destino": "",
                "precio": "",
                
            },
        };
        // Indicarle a las funciones a quién nos referimos con "this"
        this.manejarCambio = this.manejarCambio.bind(this);
        this.manejarEnvioDeFormulario = this.manejarEnvioDeFormulario.bind(this);
    }
    render() {
        return (
            <div className="col-md-5">
                <h1 className="is-size-3">Agregar viaje</h1>
                <ToastContainer></ToastContainer>
                <form className="field" onSubmit={this.manejarEnvioDeFormulario}>
                    <div className="form-group">
                        <label className="label" htmlFor="nombre">Codigo:</label>
                        <input autoFocus required placeholder="Codigo" type="text" id="codigo" onChange={this.manejarCambio} value={this.state.viajes.codigo} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label className="label" htmlFor="precio">Numero De plaza:</label>
                        <input required placeholder="Numero de palza" type="number" id="n_plaza" onChange={this.manejarCambio} value={this.state.viajes.n_plaza} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label className="label" htmlFor="calificacion">origen:</label>
                        <input required placeholder="Origen" type="text" id="origen" onChange={this.manejarCambio} value={this.state.viajes.origen} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label className="label" htmlFor="calificacion">Destino:</label>
                        <input required placeholder="Destino" type="text" id="destino" onChange={this.manejarCambio} value={this.state.viajes.destino} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label className="label" htmlFor="calificacion">Precio:</label>
                        <input required placeholder="Precio" type="text" id="precio" onChange={this.manejarCambio} value={this.state.viajes.precio} className="form-control" />
                    </div>
                    <div className="form-group">
                        <br></br>
                        <button className="btn btn-success">Guardar</button>
                        &nbsp;
                        <Link to="/Home" className="btn btn-primary">Volver</Link>
                    </div>
                </form>
            </div>
        );
    }
    async manejarEnvioDeFormulario(evento) {

        evento.preventDefault();
        // Codificar nuestro viaje como JSON

        const cargaUtil = JSON.stringify(this.state.viajes);

        const respuesta = await fetch(`/api/post_api_viajes`, {
            method: "POST",
            body: cargaUtil,
        });
        const exitoso = await respuesta.json();
        if (exitoso) {
            toast('Viaje guardado', {
                position: "top-left",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            this.setState({
                viajes: {
                    codigo: "",
                    n_plaza: "",
                    origin: "",
                    destino: "",
                    precio: "",
                }
            });
        } else {
            toast.error("Error guardando. Intenta de nuevo");
        }
    }
    manejarCambio(evento) {
        // Extraer la clave del estado que se va a actualizar, así como el valor
        const clave = evento.target.id;
        let valor = evento.target.value;
        this.setState(state => {
            const viajesactualizado = state.viajes;

            viajesactualizado[clave] = valor;
            return {
                viajes: viajesactualizado,
            }
            
        });
    }
}

export default Add;
