import React from 'react';
import { NavLink } from "react-router-dom";
import '../styles/bootstrap.min.css';

class Nav extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mostrarMenu: false,
        };
    }
    render() {
        return (
            
            <nav ClassName="navbar navbar-expand-lg navbar-light bg-light">
                <div className='container-fluid'>
                    <div className="nav-link">
                        <NavLink   className="navbar-item" to="/">Inicio</NavLink> /
                        <NavLink   className="navbar-item" to="/listar"> Listar viajes</NavLink> /
                        <NavLink   className="navbar-item" to="/add"> Agregar</NavLink> /
                        <NavLink   className="navbar-item" to="/addviajes"> Tomar un vuelo</NavLink> / 
                        <NavLink   className="navbar-item" to="/listarviajeros"> Listar Viajeros</NavLink>
                    </div>
                </div>
            </nav>
        );
    }
}
export default Nav;

