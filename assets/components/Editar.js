import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import '../styles/bootstrap.min.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'

class Editar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            viajes: {
                "id" : "",
                "codigo": "",
                "n_plaza": "",
                "origen": "",
                "destino": "",
                "precio": "",
                
            },
        };
        // Indicarle a las funciones a quién nos referimos con "this"
        this.manejarCambio = this.manejarCambio.bind(this);
        this.manejarEnvioDeFormulario = this.manejarEnvioDeFormulario.bind(this);
    }

    async componentDidMount() {
        // Obtener ID de URL
        const id = this.props.match.params.id;
        //console.log(id);
        // Llamar a la API para obtener los detalles
        const respuesta = await fetch(`api/obt_api_viajes/${id}`);
        const viajes = await respuesta.json();
        // "refrescar" el formulario
        this.setState({
            viajes: viajes,
        });
    }
    render() {
        return (
            <div className="col-md-5">
                <h1 className="is-size-3">Editar viaje</h1>
                <ToastContainer></ToastContainer>
                <form className="field" onSubmit={this.manejarEnvioDeFormulario}>
                    <div className="form-group">
                        <label className="label">Codigo:</label>
                        <input type="hidden" id="id" onChange={this.manejarCambio} value={this.state.viajes.id} className="form-control" />
                        <input autoFocus required placeholder="Codigo" type="text" id="codigo" onChange={this.manejarCambio} value={this.state.viajes.codigo} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label className="label">Numero De plaza:</label>
                        <input required placeholder="Numero de palza" type="number" id="n_plaza" onChange={this.manejarCambio} value={this.state.viajes.n_plaza} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label className="label">origen:</label>
                        <input required placeholder="Origen" type="text" id="origen" onChange={this.manejarCambio} value={this.state.viajes.origen} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label className="label">Destino:</label>
                        <input required placeholder="Destino" type="text" id="destino" onChange={this.manejarCambio} value={this.state.viajes.destino} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label className="label">Precio:</label>
                        <input required placeholder="Precio" type="text" id="precio" onChange={this.manejarCambio} value={this.state.viajes.precio} className="form-control" />
                    </div>
                    <div className="form-group">
                        <br></br>
                        <button className="btn btn-success">Guardar</button>
                        &nbsp;
                        <Link to="/" className="btn btn-primary">Volver</Link>
                    </div>
                </form>
            </div>
        );
    }
    async manejarEnvioDeFormulario(evento) {

        evento.preventDefault();
        // Codificar nuestro viaje como JSON

        const cargaUtil = JSON.stringify(this.state.viajes);

        const respuesta = await fetch(`/api/update_api_viajes/${this.state.viajes.id}`, {
            method: "PUT",
            body: cargaUtil,
        });
        const exitoso = await respuesta.json();
        if (exitoso) {
            toast('Viaje guardado', {
                position: "top-left",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            this.setState({
                viajes: {
                    codigo: "",
                    n_plaza: "",
                    origin: "",
                    destino: "",
                    precio: "",
                }
            });
        } else {
            toast.error("Error guardando. Intenta de nuevo");
        }
    }
    manejarCambio(evento) {
        // Extraer la clave del estado que se va a actualizar, así como el valor
        const clave = evento.target.id;
        let valor = evento.target.value;
        this.setState(state => {
            const viajesactualizado = state.viajes;

            viajesactualizado[clave] = valor;
            return {
                viajes: viajesactualizado,
            }
            
        });
    }
}

export default withRouter(Editar);
