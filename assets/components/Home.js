import React from "react";
import {Switch,Route,} from "react-router-dom";
import Nav from "./Nav";
import Listar from './Listar';
import Editar from "./Editar";
import Add from "./Add";
import AddViajes from "./AddViajes";
import ListarViajero from "./ListarViajeros";
import EditarViajeros from "./EditarViajeros";
import '../styles/bootstrap.min.css';

function Home() {
    return (
      <div>
        <Nav></Nav>
        <div className="row">
          <div className="container">
            <Switch>
                <Route path="/listar">
                    <Listar></Listar>
                </Route>  
                <Route path="/add">
                    <Add></Add>
                </Route>
                <Route path="/editar/:id">
                    <Editar></Editar>
                </Route>
                <Route path="/addviajes">
                    <AddViajes></AddViajes>
                </Route>
                <Route path="/listarviajeros">
                    <ListarViajero></ListarViajero>
                </Route>
                <Route path="/editarviajero/:id">
                    <EditarViajeros></EditarViajeros>
                </Route>
            </Switch>
           
          </div>
        </div>
      </div>
      
    );
  }
  
  export default Home;