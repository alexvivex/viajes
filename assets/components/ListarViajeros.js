
import React from 'react';
import '../styles/bootstrap.min.css';
import FilatableViajeros from './FilatableViajeros';


class ListarViajeros extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listarviajeros: [],
        };
    }
    async componentDidMount() {
        const respuesta = await fetch('/api/get_api_viajeros');
        const listarviajeros = await respuesta.json();
        this.setState({
            listarviajeros: listarviajeros,
        });
    }
    render() {
        return (
            <div>
                <div className='container'>
                    <div className="col-md-12">
                       
                    </div>
                </div>
                <hr>
                </hr>
                <br></br>
                <h1 className="col-md-5">Listar Viajeros</h1>
                <div className="table-container">
                    <table className="table is-fullwidth is-bordered">
                        <thead>
                            <tr>
                                <th>Cédula</th>
                                <th>Nombre</th>
                                <th>Telefono</th>
                                <th>Fecha</th>
                                <th>Destino del Viaje</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.listarviajeros.map(listarviajeros => {
                                return <FilatableViajeros key={listarviajeros.id} listarviajeros={listarviajeros}></FilatableViajeros>;
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default ListarViajeros;
