import React from 'react';
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';

class FilatableViajeros extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            eliminadov: false,
        };
        this.eliminarv = this.eliminarv.bind(this);
    }
        async eliminarv() {
            const resultado = await Swal.fire({
                title: 'Confirmación',
                text: `¿Eliminar "${this.props.listarviajeros.id}"?`,
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3298dc',
                cancelButtonColor: '#f14668',
                cancelButtonText: 'No',
                confirmButtonText: 'Sí, eliminar'
            });
            // Si no confirma, detenemos la función
            if (!resultado.value) {
                return;
            }
             
            const respuesta = await fetch(`/api/delete_api/${this.props.listarviajeros.id}`, {
                method: "DELETE",
            });

            const exitoso = await respuesta.json();
            if (exitoso) {
                toast('Viajero eliminado ', {
                    position: "top-left",
                    autoClose: 2000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
                this.setState({
                    eliminarv: true,
                });
            } else {
                toast.error("Error eliminando. Intenta de nuevo");
            }
        }
    render() {
        if (this.state.eliminadov) {
            return null;
        }
        const date = new Date();
        const dateAsString = date.toString();
        return (
            
            <tr>
                <td>{this.props.listarviajeros.cedula}</td>
                <td>{this.props.listarviajeros.nombre}</td>
                <td>{this.props.listarviajeros.telefono}</td>
                <td>{this.props.listarviajeros.fecha.date}</td>
                <td>{this.props.listarviajeros.origen + "---" + this.props.listarviajeros.destino }</td>
                <td>
                <td>
                    <Link to={`/editarviajero/${this.props.listarviajeros.id}`} className="btn btn-info">Editar</Link>
                </td>
                </td>
                <td>
                    <button onClick={this.eliminarv} className="btn btn-danger">Eliminar</button>
                </td>
            </tr>
        );
    }
}

export default FilatableViajeros;