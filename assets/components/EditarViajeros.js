import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import '../styles/bootstrap.min.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'

class EditarViajeros extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listarv: [],
            viajeros: {
                "id" : "",
                "cedula": "",
                "nombre": "",
                "telefono": "",
                "id_viaje": "",   
            },
        };
        // Indicarle a las funciones a quién nos referimos con "this"
        this.manejarCambio = this.manejarCambio.bind(this);
        this.manejarEnvioDeFormulario = this.manejarEnvioDeFormulario.bind(this);
        
    }
    async componentDidMount() {
        // Obtener ID de URL
        const id = this.props.match.params.id;
        //console.log(id);
        // Llamar a la API para obtener los detalles
        const respuesta = await fetch(`api/get_api_personas/${id}`);
        const viajeros = await respuesta.json();

        const respuesta1 = await fetch('/api/get_api_viajes');
        const listarv = await respuesta1.json();
        // "refrescar" el formulario
        this.setState({
            viajeros: viajeros,
        });

        this.setState({
            listarv: listarv,
        });
    }
    render() {
        return (
            <div className="col-md-5">
                <h1 className="is-size-3">Editar Viajero</h1>
                <ToastContainer></ToastContainer>
                <form className="field" onSubmit={this.manejarEnvioDeFormulario}>
                    <div className="form-group">
                        <label className="label">Cédula:</label>
                        <input type="hidden" id="id" onChange={this.manejarCambio} value={this.state.viajeros.id} className="form-control" />
                        <input autoFocus required placeholder="cedula" type="text" id="cedula" onChange={this.manejarCambio} value={this.state.viajeros.cedula} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label className="label">Nombre:</label>
                        <input required placeholder="Nombre" type="text" id="nombre" onChange={this.manejarCambio} value={this.state.viajeros.nombre} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label className="label">Telefono:</label>
                        <input required placeholder="Telefono" type="text" id="telefono" onChange={this.manejarCambio} value={this.state.viajeros.telefono} className="form-control" />
                    </div>

                    <div className="form-group">
                        <label className="label">Vuelo:</label>
                        <select name="id_viaje" onChange={this.manejarCambio} id="id_viaje"  className='form-control'>
                            {this.state.listarv.map((listarv) => (
                            <option value={listarv.id}>{listarv.origen + "-" + listarv.destino}</option>
                        ))}
                        </select>
                    </div>

                    <div className="form-group">
                        <br></br>
                        <button className="btn btn-success">Guardar</button>
                        &nbsp;
                        <Link to="/" className="btn btn-primary">Volver</Link>
                    </div>
                </form>
            </div>
        );
    }
    async manejarEnvioDeFormulario(evento) {

        evento.preventDefault();
        // Codificar nuestro viaje como JSON

        const cargaUtil = JSON.stringify(this.state.viajeros);

        const respuesta = await fetch(`/api/update_api/${this.state.viajeros.id}`, {
            method: "PUT",
            body: cargaUtil,
        });
        const exitoso = await respuesta.json();
        if (exitoso) {
            toast('Viaje guardado', {
                position: "top-left",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            this.setState({
                viajeros: {
                    cedula: "",
                    nombre: "",
                    telefono: "",
                    id_viaje: "",
                }
            });
        } else {
            toast.error("Error guardando. Intenta de nuevo");
        }
    }
    manejarCambio(evento) {
        // Extraer la clave del estado que se va a actualizar, así como el valor
        const clave = evento.target.id;
        let valor = evento.target.value;
        this.setState(state => {
            const viajerosactualizado = state.viajeros;

            viajerosactualizado[clave] = valor;
            return {
                viajeros: viajerosactualizado,
            }
            
        });
    }
}

export default withRouter(EditarViajeros);
