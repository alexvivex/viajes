<?php

namespace App\Controller;

use App\Entity\Pasajeros;
use App\Entity\Viajes;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DriverManager;





class ApiController extends AbstractController
{
    /**
     * @Route("/api", name="app_api")
     */
    public function index(): Response
    {
        $msj = 'Hola Alexis';
        return $this->json([$msj]);
    }

    /**
     * @Route("/api/post_api", name="post_api", methods={"POST"})
     */
    public function post_api(Request $request): Response
    { 
        $pasajeros = new Pasajeros();
        $parametros = json_decode($request->getContent(), true);
        
        $pasajeros->setCedula($parametros['cedula']);
        $pasajeros->setNombre($parametros['nombre']);
        $date = new \DateTime($parametros['fecha']);
        $pasajeros->setFecha($date);
        $pasajeros->setTelefono($parametros['telefono']);
        $pasajeros->setid_viaje($parametros['id_viaje']);

        $data = $this->getDoctrine()->getManager();
        $data->persist($pasajeros);
        $data->flush();

        //dd($parametros);
        return $this->json("Insertado con exito");
    }

     /**
     * @Route("/api/update_api/{id}", name="update_api", methods={"PUT"})
     */
    public function update_api(Request $request, $id): Response
    { 
        $data = $this->getDoctrine()->getRepository(Pasajeros::class)->find($id);
        //dd($data);
        $parametros = json_decode($request->getContent(), true);
        
        $data->setCedula($parametros['cedula']);
        $data->setNombre($parametros['nombre']);
        $data->setTelefono($parametros['telefono']);
        $data->setid_viaje($parametros['id_viaje']);

        $dataUpadate = $this->getDoctrine()->getManager();
        $dataUpadate->persist($data);
        $dataUpadate->flush();

        
        return $this->json("Actualizado con exito");
    }

    /**
     * @Route("/api/delete_api/{id}", name="delete_api", methods={"DELETE"})
     */

    public function delete_api($id): Response
    { 
        $data = $this->getDoctrine()->getRepository(Pasajeros::class)->find($id);

        $dataDelete = $this->getDoctrine()->getManager();
        $dataDelete->remove($data);
        $dataDelete->flush();

        return $this->json("Eliminado con exito");
    }

     /**
     * @Route("/api/get_api", name="get_api", methods={"GET"})
     */

    public function get_api(): Response
    { 
        $data = $this->getDoctrine()->getRepository(Pasajeros::class)->findAll();
        foreach($data as $row)
        {
            $result[] = [
                'id' => $row->getId(),
                'cedula' => $row->getCedula(),
                'nombre' => $row->getNombre(),
                'telefono' => $row->getTelefono()
            ];
        }
        return $this->json($result);
    }

    /**
     * @Route("/api/get_api_personas/{id}", name="get_api_personas", methods={"GET"})
     */
     public function get_api_personas($id): Response
    { 
            $data = $this->getDoctrine()->getRepository(Pasajeros::class)->find($id);

            $result = [
                'id' => $data->getId(),
                'cedula' => $data->getCedula(),
                'nombre' => $data->getNombre(),
                'telefono' => $data->getTelefono()
            ];
        return $this->json($result);
    }


     //API crud Viajes//

    /**
     * @Route("/api/post_api_viajes", name="post_api_viajes", methods={"POST"})
     */
    public function post_api_viajes(Request $request): Response
    { 
        $viajes = new Viajes();
        $parametros = json_decode($request->getContent(), true);
        
        $viajes->setCodigo($parametros['codigo']);
        $viajes->setNPlaza($parametros['n_plaza']);
        $viajes->setOrigen($parametros['origen']);
        $viajes->setDestino($parametros['destino']);
        $viajes->setPrecio($parametros['precio']);

        $data = $this->getDoctrine()->getManager();
        $data->persist($viajes);
        $data->flush();

        //dd($parametros);
        return $this->json("Viajes Insertado con exito");
    }

     /**
     * @Route("/api/update_api_viajes/{id}", name="update_api_viajes", methods={"PUT"})
     */
    public function update_api_viajes(Request $request, $id): Response
    { 
        $data = $this->getDoctrine()->getRepository(Viajes::class)->find($id);
        //dd($data);
        $parametros = json_decode($request->getContent(), true);
        
        $data->setCodigo($parametros['codigo']);
        $data->setNPlaza($parametros['n_plaza']);
        $data->setOrigen($parametros['origen']);
        $data->setDestino($parametros['destino']);
        $data->setPrecio($parametros['precio']);

        $viajesUpadate = $this->getDoctrine()->getManager();
        $viajesUpadate->persist($data);
        $viajesUpadate->flush();

        
        return $this->json("Viajes Actualizado con exito");
    }

    /**
     * @Route("/api/delete_api_viajes/{id}", name="delete_api_viajes", methods={"DELETE"})
     */

    public function delete_api_viajes($id): Response
    { 
        $data = $this->getDoctrine()->getRepository(Viajes::class)->find($id);

        $dataDelete = $this->getDoctrine()->getManager();
        $dataDelete->remove($data);
        $dataDelete->flush();

        return $this->json("Viaje Eliminado con exito");
    }

     /**
     * @Route("/api/obt_api_viajes/{id}", name="obt_api_viajes", methods={"GET"})
     */

    public function obt_api_viajes($id): Response
    { 
        $data = $this->getDoctrine()->getRepository(Viajes::class)->find($id);

            $result = [
                'id' => $data->getId(),
                'codigo' => $data->getCodigo(),
                'n_plaza' => $data->getNPlaza(),
                'origen' => $data->getOrigen(),
                'destino' => $data->getDestino(),
                'precio' => $data->getPrecio(),
            ];
        return $this->json($result);
    }

     /**
     * @Route("/api/get_api_viajes", name="get_api_viajes", methods={"GET"})
     */

    public function get_api_viajes(): Response
    { 
        $data = $this->getDoctrine()->getRepository(Viajes::class)->findAll();
        foreach($data as $row)
        {
            $result[] = [
                'id' => $row->getId(),
                'codigo' => $row->getCodigo(),
                'n_plaza' => $row->getNPlaza(),
                'origen' => $row->getOrigen(),
                'destino' => $row->getDestino(),
                'precio' => $row->getPrecio()
            ];
        }
        return $this->json($result);
    }

     /**
     * @Route("/api/get_api_viajeros", name="get_api_viajeros", methods={"GET"})
     */

    public function get_api_viajeros() : Response
    { 
        $p = $this->getDoctrine()->getRepository(Pasajeros::class);
        $query = $p->createQueryBuilder("p")
        ->select('p.id, p.cedula, p.nombre, p.telefono, p.fecha, v.origen,v.destino')
        ->Join('App:Viajes', 'v', 'WITH', 'v.id = p.id_viaje')
        ->getQuery()
        ->getResult();

        //dd($query);
        foreach($query as $row)
        {
            $result[] = [
                'id' => $row['id'],
                'cedula' => $row['cedula'],
                'nombre' => $row['nombre'],
                'fecha' => $row['fecha'],
                'telefono' => $row['telefono'],
                'origen' => $row['origen'],
                'destino' => $row['destino']
            ];
        }
        return $this->json($query);
    }


}
